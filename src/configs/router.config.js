const { v1ExampleRest, v1PingRest, errorHandlerRest, defaultHandlerRest } = require('../interfaces/rest');
const {
  v1ForceVATRest,
  v1ChangeEmailRest,
  v1LoanReadyForTopupRest,
  v1UpdateInstallmentStatusRest,
  v1ActivityBalanceUsersRest,
  v1ActivityMutationUsersRest,
  v1LoanStatusUsersRest,
  v1KWUsersRest,
  v1RevertLoanStatusRest,
  v1ChangeDisbursementAndDueDateRest
} = require('../interfaces/rest');
const { logger } = require('./logger.config');

const setRouter = ({ app, http, express }) => {
  const router = express.Router();

  router.use('/ping', v1PingRest);
  router.use('/test', v1ExampleRest);

  // Force VAT Change
  router.use('/force-vat-view-vat-fee', v1ForceVATRest.viewVATFee);
  router.use('/force-vat-view-vat-percentage', v1ForceVATRest.viewVATPercentage);
  router.use('/force-vat-view-request-headers', v1ForceVATRest.viewRequestHeaders);
  router.use('/force-vat-view-request-details', v1ForceVATRest.viewRequestDetails);

  // Change Email
  router.use('/change-email-check-email', v1ChangeEmailRest.viewEmail);
  router.use('/change-email-update-email', v1ChangeEmailRest.updateEmail);

  // Loan Ready for Topup
  router.use('/loan-ready-for-topup-check-loan', v1LoanReadyForTopupRest.checkLoan);
  router.use('/loan-ready-for-topup-check-loan-id', v1LoanReadyForTopupRest.checkLoanID);
  router.use('/loan-ready-for-topup-update-loan-eligible', v1LoanReadyForTopupRest.updateLoanEligibleForTopup);

  // Update Installment Status
  router.use('/update-installment-status-view-installment', v1UpdateInstallmentStatusRest.viewInstallmentStatus);
  router.use('/update-installment-status-update-installment', v1UpdateInstallmentStatusRest.updateInstallmentStatus);

  // Activity Balance Users
  router.use('/activity-view-balance-users', v1ActivityBalanceUsersRest.v1ViewBalanceUser);

  // Activity Mutation Users
  router.use('/activity-view-mutation-users', v1ActivityMutationUsersRest.v1ViewMutationUser);
  router.use('/activity-view-last-mutation-users', v1ActivityMutationUsersRest.v1ViewLastMutationUser);

  // Loan
  router.use('/koinp2p-loan-status-user', v1LoanStatusUsersRest.v1ViewLoanStatusUser);
  router.use('/koinp2p-loan-fundings-user', v1LoanStatusUsersRest.v1ViewLoanFundingsUser);

  // User
  router.use('/get-data-users', v1KWUsersRest.v1ViewDataUsers);
  router.use('/change-fullname-kw-user', v1KWUsersRest.v1UpdateFullNameUser);
  router.use('/unfreeze-kw-user', v1KWUsersRest.v1UnfreezeAccountUser);

  // Revert Loan Status
  router.use('/revert-loan-status-view-loan-status', v1RevertLoanStatusRest.checkLoanStatus);
  router.use('/revert-loan-status-update-loan-status', v1RevertLoanStatusRest.updateLoanStatus);

  // Change Disbursement and Due Date
  router.use('/change-disbursement-and-due-date-view-date', v1ChangeDisbursementAndDueDateRest.checkDisbursementAndDueDate);

  app.use(http.prefix, router);
  const listener = app.listen(http.port, () => {
    logger().info(`Server listening on port ${http.port}`)
  })
  app.all('*', defaultHandlerRest);
  app.use(errorHandlerRest);

  return listener;
}

const gracefulServerShutdown = (app) => {
  app.close(() => {
    logger().info('Terminating server gracefully');
  });
}

module.exports = { setRouter, gracefulServerShutdown };

