const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const { createLogger } = require('./logger.config');

const initMiddleware = ({app, log: env}) => {
  //application-level interceptors
  const middlewareLogger = createLogger(env.logger);
    
  //request handling 
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json({ limit: '5mb' }));
  app.use(cookieParser());
  app.use(middlewareLogger);
}

module.exports = { initMiddleware };