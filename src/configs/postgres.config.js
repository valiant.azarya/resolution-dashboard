const { Sequelize } = require('sequelize');
const { logger } = require('./logger.config');

let conn;
let conf;

const dbFactory = async (config) => {
  conf = { ...config };
  conn = getDBConn();
  await authenticate(conn);
}

const authenticate = async (conn) => {
  try {
    await conn.authenticate();
    logger().info('Connection has been established successfully.');
  } catch (err) {
    logger().error('Unable to connect to the database:', err);
  }
}

const getDBConn = () => {
  if (!conn) {
    return new Sequelize(conf.database, conf.user, conf.password, {
      host: conf.host,
      dialect: conf.dialect,
      port: conf.port,
      pool: {
        idle: conf.idle,
        evict: conf.evict,
        acquire: conf.acquire
      }
    });
  }
  return conn;
}

const gracefulDBShutdown = async () => {
  return conn.close().finally(() => {
    logger().info(`Database connection terminated gracefully`);
  });
}

module.exports = { dbFactory, getDBConn, gracefulDBShutdown };

