const express = require('express');
const app = express();

const { initMiddleware } = require('./middleware.config');
const { dbFactory, gracefulDBShutdown } = require('./postgres.config');
const { setRouter, gracefulServerShutdown } = require('./router.config');

let listener;

const initApp = (env) => {
  const postgres = {
    host: (env.POSTGRE_HOST) ? env.POSTGRE_HOST : null,
    dialect: (env.POSTGRE_DIALECT) ? env.POSTGRE_DIALECT : null,
    port: (env.POSTGRE_PORT) ? env.POSTGRE_PORT : null,
    user: (env.POSTGRE_USER) ? env.POSTGRE_USER : null,
    password: (env.POSTGRE_PASS) ? env.POSTGRE_PASS : null,
    database: (env.POSTGRE_DB_NAME) ? env.POSTGRE_DB_NAME : null,
    idle: (env.POSTGRE_POOL_IDLE) ? env.POSTGRE_POOL_IDLE : null,
    evict: (env.POSTGRE_POOL_EVICT) ? env.POSTGRE_POOL_EVICT : null,
    acquire: (env.POSTGRE_POOL_ACQUIRE) ? env.POSTGRE_POOL_ACQUIRE : null,
  };

  const http = {
    port: (env.APP_PORT) ? env.APP_PORT : null,
    prefix: (env.PREFIX) ? env.PREFIX : null,
    debug: (env.APP_DEBUG_MODE) ? env.APP_DEBUG_MODE : null,
  };

  const log = {
    logger: (env.LOGGER) ? env.LOGGER : null
  };

  initMiddleware({app, log});
  dbFactory(postgres);
  listener = setRouter({app, http, express});
}

const gracefulShutdown = async () => {
  await gracefulDBShutdown();
  await gracefulServerShutdown(listener);
}

module.exports = { initApp, gracefulShutdown };