/* eslint-disable no-console */
const moment = require('moment');
let instance;

const createLogger = (loggerLib) => {
  switch(loggerLib) {
  case 'WINSTON':
    instance = initWinston();
    break;
  default:
    instance = defaultLogger();
    break;
  }

  const logger = (req, res, next) => {
    instance.info(`REQ: [${moment().format('YYYY-MM-DD HH:mm:ss')}] ${req.method} ${req.url}`);
    next();
  }

  return logger;
}

const initWinston = () => {
  const winston = require('winston');
  const consoleTransport = new winston.transports.Console();
  const myWinstonOptions = {
    transports: [consoleTransport]
  };
  const logger = new winston.createLogger(myWinstonOptions);
  return logger;
}

const defaultLogger = () => {
  return {
    info: (obj) => {
      return console.info(obj);
    },
    warn: (obj) => {
      return console.warn(obj);
    },
    error: (obj) => {
      return console.error(obj);
    }
  }
}

const logger = () => {
  return instance;
}

module.exports = { createLogger, logger };