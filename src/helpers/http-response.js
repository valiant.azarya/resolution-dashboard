const responseJSON = (req, res, obj) => {
  res.status(obj.status).send({
    success: obj.success || false,
    data: obj.data || {},
    message: obj.message || 'Internal Error',
    stackTrace: (obj.status === 500) ? obj.err : undefined
  });
};
  
module.exports = { responseJSON };