const httpStatus = {
  success: 200,
  badRequest: 400,
  notFound: 404,
  unauthorized: 401,
  forbidden: 403,
  internalServerError: 500
};

const messages = {
  healthy: 'Service Healthy',
  success: 'OK',
  unauthorized: 'Credentials Unauthorized',
  notFound: 'Not found',
  pathNotFound: 'Requested path not found',
  invalidToken: 'Token invalid',
  secretKeyNotFound: 'Secret key not found',
  tokenExpired: 'Token expired',
  invalidAppClientId: 'Invalid app client id',
  tokenVerificationFailed: 'Signature verification failed',
  authorizationNotFound: 'Authorization header not found',
};

module.exports = { httpStatus, messages };