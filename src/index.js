const dotenv = require('dotenv');
const { initApp, gracefulShutdown } = require('./configs/init.config');

dotenv.config();

initApp(process.env);

process.on('SIGTERM', () => {
  gracefulShutdown();
});