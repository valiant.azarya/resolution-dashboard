const { logger } = require('../../configs/logger.config');
const { v1LoanService, v1ActivityService, v1AssessmentService } = require('../../infrastructures/postgres/asgard/services');

const v1RevertLoanStatusUsecase = {
  async viewLoanStatus(payload) {
    let param = payload;
    let result = [];
    
    const foundLoanbyLoanCode = await v1LoanService.getLoanStatusbyLoanCode(param.loan_code);
    let result_1 = { loan: {} };
    result_1.loan = Object.assign(result_1.loan, foundLoanbyLoanCode[0][0]);

    const foundActivitybyLoanCode = await v1ActivityService.getInternalTransferStatusbyLoanCode(param.loan_code);
    let result_2 = { activtiy: {} };
    result_2.activtiy = Object.assign(result_2.activtiy, (result_1.loan ? foundActivitybyLoanCode[0] : null));

    const foundAssessmentbyLoanCode = await v1AssessmentService.getAssessmentStatusbyLoanCode(param.loan_code);
    let result_3 = { assessment: {} };
    result_3.assessment = Object.assign(result_3.assessment, (result_1.loan ? foundAssessmentbyLoanCode[0] : null));

    result = result.concat(result_1, result_2, result_3);

    return result;
  },
  async updateLoanStatus(payload) {
    let param = payload;
    const result = await v1LoanService.updateLoanStatus(param.loan_id, param.loan_status, param.ticket);
    // const result1 = await v1ActivityService.updateInternalTransferStatus(param.internal_transfer_id, param.internal_transfer_status, param.ticket);
    // const result2 = await v1AssessmentService.updateAssessmentStatus(param.assessment_id, param.assessment_status, param.ticket);

    return result;
  }
}

module.exports = { 
  v1RevertLoanStatusUsecase,
};