const { logger } = require('../../configs/logger.config');
const { v1ForceVATService } = require('../../infrastructures/postgres/asgard/services');

const v1ForceVATUsecase = {
  async viewVATFee(payload) {
    let param = payload;
    const result = await v1ForceVATService.findLoanDeductionDetailsByLoanCode(param.loan_code);

    return result;
  },
  async viewVATPercentage(payload) {
    let param = payload;
    const result = await v1ForceVATService.findInternalTransfersDetailsByLoanID(param.loan_id);

    return result;
  },
  async viewRequestHeaders(payload) {
    let param = payload;
    const result = await v1ForceVATService.findRequestHeadersByLoanID(param.loan_id);

    return result;
  },
  async viewRequestDetails(payload) {
    let param = payload;
    const result = await v1ForceVATService.findVATFeeInRequestDetailsByLoanID(param.loan_id);

    return result;
  },
  async updateLoanDeductionDetails(payload) {
    let param = payload;
    const result = await v1ForceVATService.updateLoanDeductionDetails(param.loan_id);

    return result;
  },
  async updateDeductionAndDisbursementAmount(payload) {
    let param = payload;
    const result = await v1ForceVATService.updateDeductionAndDisbursementAmount(param.loan_id);

    return result;
  },
  async updateInternalTransferDetails(payload) {
    let param = payload;
    const result = await v1ForceVATService.updateInternalTransferDetails(param.loan_id);

    return result;
  },
  async updateCashInOutDetails(payload) {
    let param = payload;
    const result = await v1ForceVATService.updateCashInOutDetails(param.loan_id);

    return result;
  },
}

module.exports = { 
  v1ForceVATUsecase
};