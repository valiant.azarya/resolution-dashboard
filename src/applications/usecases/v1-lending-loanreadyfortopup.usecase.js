const { logger } = require('../../configs/logger.config');
const { v1LoanService } = require('../../infrastructures/postgres/asgard/services');

const v1LoanReadyForTopupUsecase = {
  async viewLoan(payload) {
    let param = payload;
    let result = [];
    
    const foundLoanbyUserEmail = await v1LoanService.findLoanbyUserEmail(param.email);
    let result_1 = { loans: {} };
    result_1.loans = Object.assign(result_1.loans, foundLoanbyUserEmail[0]);

    result = result.concat(result_1);

    return result;
  },
  async viewLoanID(payload) {
    let param = payload;
    let result = [];

    const foundLoanbyLoanID = await v1LoanService.findLoanbyLoanID(param.loan_id);
    let result_2 = { loan_detail: {} };
    result_2.loan_detail = Object.assign(result_2.loan_detail, foundLoanbyLoanID[0]);

    const foundLoanRepaymentSchedulesbyLoanID = await v1LoanService.findLoanRepaymentSchedulesbyLoanID(param.loan_id);
    let result_3 = { loan_schedules: {} };
    result_3.loan_schedules = Object.assign(result_3.loan_schedules, (result_2.loan_detail ? foundLoanRepaymentSchedulesbyLoanID[0] : null));
    
    const foundRejectedLoanbyLoanID = await v1LoanService.findRejectedLoanbyLoanID(param.email, param.loan_id);
    let result_4 = { rejected_loan: {} };
    result_4.rejected_loan = Object.assign(result_4.rejected_loan, (foundRejectedLoanbyLoanID[0] ? foundRejectedLoanbyLoanID[0] : null));

    result = result.concat(result_2, result_3, result_4);

    return result;
  },
  async updateLoanEligibleForTopup(payload) {
    let param = payload;
    const result = await v1LoanService.updateLoanEligibleForTopUp(param.loan_id, param.ticket);

    return result;
  },
  async updateLoanTopupRejected(payload) {
    let param = payload;
    const result = await v1LoanService.updateLoanTopupRejected(param.loan_id, param.loan_topup_request_id, param.ticket);

    return result;
  }
}

module.exports = { 
    v1LoanReadyForTopupUsecase,
  };