const { v1ExampleService } = require('../../infrastructures/postgres/asgard/services');

const v1ExampleUsecase = async (payload) => {
  // all the business logic happens here
  let param = payload;

  // get the impacted users

  const result = await v1ExampleService.findUsersWithEmptyNPWP(param);

  return result;
};

module.exports = { v1ExampleUsecase };