const { v1ExampleUsecase } = require('./v1-examplecategory-examplesubcategory.usecase');
const { v1ForceVATUsecase } = require('./v1-lending-forcevat.usecase');
const { v1ChangeEmailUsecase } = require('./v1-lending-changeemail.usecase');
const { v1LoanReadyForTopupUsecase } = require('./v1-lending-loanreadyfortopup.usecase');
const { v1UpdateInstallmentStatusUsecase } = require('./v1-lending-updateinstallmentstatus.usecase');
const { v1PlatformActivityBalanceUseCase } = require('./v1-platform-activity-balance.usescase');
const { v1PlatformActivityMutationUseCase } = require('./v1-platform-activity-mutation.usecase');
const { v1KoinP2PLoanStatusUserUseCase } = require('./v1-koinp2p-loan-status-user.usecase');
const { v1KWUsersUseCase } = require('./v1-kw-users.usecase');
const { v1RevertLoanStatusUsecase } = require('./v1-lending-revertloanstatus.usecase');
const { v1ChangeDisbursementAndDueDateUsecase } = require('./v1-lending-changedisbursementandduedate.usecase');

module.exports = {
    v1ExampleUsecase,
    v1ForceVATUsecase,
    v1ChangeEmailUsecase,
    v1LoanReadyForTopupUsecase,
    v1UpdateInstallmentStatusUsecase,
    v1PlatformActivityBalanceUseCase,
    v1PlatformActivityMutationUseCase,
    v1KoinP2PLoanStatusUserUseCase,
    v1KWUsersUseCase,
    v1RevertLoanStatusUsecase,
    v1ChangeDisbursementAndDueDateUsecase
};
