const { v1BalanceUsersService } = require('../../infrastructures/postgres/asgard/services');

const v1PlatformActivityBalanceUseCase = {
  async viewBalanceUser(payload) {
    let param = payload;
    const result = await v1BalanceUsersService.findBalanceUser(param.user_code);
    return result;
  }
}

module.exports = { v1PlatformActivityBalanceUseCase };