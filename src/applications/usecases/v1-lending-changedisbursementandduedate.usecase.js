const { logger } = require('../../configs/logger.config');
const { v1LoanService, v1ActivityService, v1PortfolioService } = require('../../infrastructures/postgres/asgard/services');

const v1ChangeDisbursementAndDueDateUsecase = {
  async viewLoanDisbursementAndDueDate(payload) {
    let param = payload;
    let result = [];
    
    const foundLoanDisbursementDatebyLoanCode = await v1LoanService.getLoanDisbursementDatebyLoanCode(param.loan_code);
    let result_1 = { loan_disbursement_date: {} };
    result_1.loan_disbursement_date = Object.assign(result_1.loan_disbursement_date, foundLoanDisbursementDatebyLoanCode[0][0]);

    const foundLoanRepaymentSchedulebyLoanCode = await v1LoanService.getLoanRepaymentSchedulebyLoanCode(param.loan_code);
    let result_2 = { loan_repayment_schedule: {} };
    result_2.loan_repayment_schedule = Object.assign(result_2.loan_repayment_schedule, (result_1.loan_disbursement_date ? foundLoanRepaymentSchedulebyLoanCode[0] : null));

    const foundLoanLenderSchedulebyLoanCode = await v1LoanService.getLoanLenderSchedulebyLoanCode(param.loan_code);
    let result_3 = { loan_lender_schedule: {} };
    result_3.loan_lender_schedule = Object.assign(result_3.loan_lender_schedule, (result_1.loan_disbursement_date ? foundLoanLenderSchedulebyLoanCode[0] : null));

    const foundActivityDisbursementDateAndDueDatebyLoanCode = await v1ActivityService.getDisbursementDateAndDueDatebyLoanCode(param.loan_code);
    let result_4 = { activity_disbursement_date_and_due_date: {} };
    result_4.activity_disbursement_date_and_due_date = Object.assign(result_4.activity_disbursement_date_and_due_date, (result_1.loan_disbursement_date ? foundActivityDisbursementDateAndDueDatebyLoanCode[0] : null));

    const foundPortfolioRepaymentSchedulebyLoanCode = await v1PortfolioService.getRepaymentSchedulebyLoanCode(param.loan_code);
    let result_5 = { portfolio_repayment_schedule: {} };
    result_5.portfolio_repayment_schedule = Object.assign(result_5.portfolio_repayment_schedule, (result_1.loan_disbursement_date ? foundPortfolioRepaymentSchedulebyLoanCode[0] : null));

    result = result.concat(result_1, result_2, result_3, result_4, result_5);

    return result;
  }
}

module.exports = { 
  v1ChangeDisbursementAndDueDateUsecase,
};
