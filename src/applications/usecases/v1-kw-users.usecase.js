const { v1KWUserService } = require('../../infrastructures/postgres/asgard/services');

const v1KWUsersUseCase = {
  async viewDataKWUsers(payload) {
    let param = payload;
    const result = await v1KWUserService.findDataUser(param.user_code);
    return result;
  },
  async updateFullNameUser(payload) {
    let param = payload;
    const result = await v1KWUserService.updateFullNameUser(param.user_code, param.fullname, param.ticket);
    return result;
  },
  async unfreezeAccountUser(payload) {
    let param = payload;
    const result = await v1KWUserService.unfreezeAccountUser(param.user_code, param.ticket);
    return result;
  }
}

module.exports = {
  v1KWUsersUseCase
};