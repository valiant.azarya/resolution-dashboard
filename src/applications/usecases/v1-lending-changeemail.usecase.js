const { logger } = require('../../configs/logger.config');
const { v1EmailService } = require('../../infrastructures/postgres/asgard/services');

const v1ChangeEmailUsecase = {
  async viewEmail(payload) {
    let param = payload;
    const result = await v1EmailService.findEmailByEmail(param.email);

    return result;
  },
  async updateEmail(payload) {
    let param = payload;
    const result = await v1EmailService.updateEmail(param.old_email, param.new_email, param.ticket);

    // if (result.user_id == )
  }
}

module.exports = { 
    v1ChangeEmailUsecase,
  };