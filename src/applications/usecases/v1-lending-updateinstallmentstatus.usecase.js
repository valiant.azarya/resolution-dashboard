const { logger } = require('../../configs/logger.config');
const { v1LoanService } = require('../../infrastructures/postgres/asgard/services');

const v1UpdateInstallmentStatusUsecase = {
  async viewLoansRepaymentSchedules(payload) {
    let param = payload;
    const result = await v1LoanService.findLoansRepaymentSchedules();

    return result;
  },
  async updateInstallmentStatus(payload) {
    let param = payload;
    const resultPre = await v1LoanService.updateRepaymentSchedule(param.ticket);

    let result;
    if (resultPre) {
      result = await v1LoanService.updateLenderSchedule(param.schedule_ids, param.ticket);
    }

    return result;
  }
}

module.exports = { 
  v1UpdateInstallmentStatusUsecase,
  };