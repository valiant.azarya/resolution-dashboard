const { v1MutationUsersService } = require('../../infrastructures/postgres/asgard/services');

const v1PlatformActivityMutationUseCase = {
  async viewMutation(payload) {
    let param = payload;
    let limit = 10;
    if (param.limit) {
      limit = param.limit;
    }
    const result = await v1MutationUsersService.findMutationUser(param.user_code, limit, param);
    return result;
  },
  async viewLastMutation(payload) {
    let param = payload;
    let limit = 10;
    if (param.limit) {
      limit = param.limit;
    }
    const result = await v1MutationUsersService.findLastTransactionUser(param.user_code, limit);
    return result;
  }
}

module.exports = { v1PlatformActivityMutationUseCase };