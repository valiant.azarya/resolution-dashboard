const { v1LoanUserService } = require('../../infrastructures/postgres/asgard/services');

const v1KoinP2PLoanStatusUserUseCase = {
  async viewLoanStatusUser(payload) {
    let param = payload;
    const result = await v1LoanUserService.findLoanStatusUser(param.user_id, param.order_code);
    return result;
  },
  async viewLoanFundingsUser(payload) {
    let param = payload;
    const result = await v1LoanUserService.findLoanFundingsUser(param.user_id, param.loan_id);
    return result;
  }
}

module.exports = { v1KoinP2PLoanStatusUserUseCase };