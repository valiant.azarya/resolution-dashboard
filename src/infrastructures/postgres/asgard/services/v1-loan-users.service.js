const { QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1LoanUserService = {
  async findLoanStatusUser(user_id, order_code) {
    try {
      const dbConn = getDBConn();
      const sql = `
        select ap.param_value, o.process_status, od.*
        from koinp2p.orders o 
        join koinp2p.admin_params ap on ap.admin_param_uuid  = o.order_status 
        join koinp2p.order_details od on od.order_id = o.order_id 
        where 
            o.user_id = :user_id and 
            o.order_code  = :order_code and
            o.is_active  = '1' and 
            od.is_active  = '1'; `;
      result = await dbConn.query(sql, {
        replacements: { user_id: user_id, order_code: order_code },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  },
  async findLoanFundingsUser(user_id, loan_id) {
    try {
      const dbConn = getDBConn();
      const sql = `
        select * from loans.loan_fundings lf
        where 
          lf.user_id = :user_id and 
          loan_id = :loan_id ; `;
      result = await dbConn.query(sql, {
        replacements: { user_id: user_id, loan_id: loan_id },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  }
};

module.exports = { v1LoanUserService };