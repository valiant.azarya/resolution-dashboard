const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1AssessmentService = {
  async getAssessmentStatusbyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          a. assessment_id,
          a.assessment_available_at,
          ap_type.description as assessment_type,
          a.assessed_id,
          ap_by_division.description as assessed_by_division,
          a.assessed_by,
          a.assessed_at,
          a.finished_assessed_at,
          s_ap_result.description as system_assessment_result, 
          m_ap_result.description as manual_assessment_result,
          a.result_explanation,
          ap_product.description as product,
          ap_fraud.description as auto_fraud 
        from assessments.assessment a 
        left join assessments.admin_params s_ap_result on a.system_assessment_result = s_ap_result.admin_param_uuid 
        left join assessments.admin_params m_ap_result on a.system_assessment_result = m_ap_result.admin_param_uuid 
        left join assessments.admin_params ap_type on a.assessment_type = ap_type.admin_param_uuid 
        left join assessments.admin_params ap_by_division on a.assessed_by_division = ap_by_division.admin_param_uuid 
        left join assessments.admin_params ap_product on a.product = ap_product.admin_param_uuid 
        left join assessments.admin_params ap_fraud on a.auto_fraud = ap_fraud.admin_param_uuid 
        where a.is_active = '1' and
              a.loan_code = :loan_code;
      `,{
        replacements: { loan_code: loan_code },
        query: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateAssessmentStatus(assessment_id, assessment_status, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update assessments.assessment
        set system_assessment_result = :assessment_status,
          manual_assessment_result = :assessment_status,
          modified_by = :ticket,
          modified_at = now()
        where assessment_id = :assessment_id;
      `, { 
        replacements: { ticket: ticket, assessment_id: assessment_id, assessment_status: assessment_status },
        type: QueryTypes.UPDATE,
        transaction: t 
      }) //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  }
};

module.exports = { v1AssessmentService };