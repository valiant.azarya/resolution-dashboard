const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1EmailService = {
  async findEmailByEmail(email) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`select user_id from users.users where email = :email`,
      {
        replacements: { email: email },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateEmail(old_email, new_email, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        UPDATE users.users
        SET email=:new_email,
            modified_by=:ticket,
            modified_at=now()
        WHERE email=:old_email;
      `, { 
        replacements: { new_email: new_email, old_email: old_email, ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t
      }); //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      if(transaction) {
        await transaction.rollback();
      }
      throw(err);
    }
  }
};

module.exports = { v1EmailService };