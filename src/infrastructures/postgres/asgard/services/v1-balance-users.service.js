const { QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1BalanceUsersService = {
  async findBalanceUser(user_code) {
    try {
      const dbConn = getDBConn();
      const sql = `
            SELECT 
                usr.user_id, usr.user_code, usr.email,
                ab.available_cash, ab.available_koin, 
                ab.pending_mutation_cash, ab.pending_mutation_koin,
                base_cash, base_koin,
                ab.modified_by, ab.modified_at
            FROM activity.account_balances ab 
            INNER JOIN users.users usr
            ON ab.user_id = usr.user_id
            where usr.user_code = :user_code `;
      result = await dbConn.query(sql, {
        replacements: { user_code: user_code },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  }
};

module.exports = { v1BalanceUsersService };