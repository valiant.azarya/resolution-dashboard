const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1ActivityService = {
  async getInternalTransferStatusbyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          it.loan_id,
          ap_status.description as internal_transfer_status,
          amount,
          deduction,
          requested_at,
          approved_at 
        from activity.internal_transfers it 
        left join loans.loans l on it.loan_id = l.loan_id 
        left join activity.admin_params ap_status on it.internal_transfer_status = ap_status.admin_param_uuid 
        where l.is_active = '1' and
            l.loan_code = :loan_code;
      `,{
        replacements: { loan_code: loan_code },
        query: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateInternalTransferStatus(internal_transfer_id, internal_transfer_status, ticket) {
    const t = await Sequelize.Transaction();
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        update activity.internal_transfers
        set internal_transfer_status = :internal_transfer_status,
          modified_by = :ticket,
          modified_at = now()
        where internal_transfer_id = :internal_transfer_id;
      `, { 
        replacements: { ticket: ticket, internal_transfer_id: internal_transfer_id, internal_transfer_status: internal_transfer_status },
        query: QueryTypes.UPDATE,
        transaction: t 
      }) //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async getDisbursementDateAndDueDatebyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          ciord.detail_name,
          ciord.detail_value 
        from 
          activity.cash_in_out_request_details ciord 
        where (
          (detail_name='transfer_date' or detail_name='loan_first_due_date')
          and is_active='1'
          and in_out_header_id = (
            select 
                in_out_header_id 
            from activity.cash_in_out_request_headers ciorh  
            where 
              reference_id = (select l.loan_id from loans.loans l where l.loan_code = :loan_code)
              and in_out_type  = '3b6b220b-2019-4279-8343-cbe6167f7511')
        );
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  }
};

module.exports = { v1ActivityService };