const { getDBConn } = require("../../../../configs/postgres.config");
const { Sequelize, QueryTypes } = require('sequelize');

const v1LoanService = {
  async findLoanbyUserEmail(email) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        SELECT
          l.loan_id,
          l.loan_code,
          l.is_eligible_for_top_up,
          l.is_top_up,
          (
            SELECT
              param_value
            FROM
              loans.admin_params ap
            WHERE
              ap.admin_param_uuid = l.loan_status),
          l.applied_at
        FROM
          loans.loans l
        WHERE
          l.user_id = (
            SELECT
              user_id
            FROM
              users.users u
            WHERE
              u.email = :email)
        ORDER BY
          created_at DESC;
      `,{
          replacements: { email: email }
          // type: QueryTypes.SELECT
        }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async findLoanbyLoanID(loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select loan_id,
        old_loan_id,
        user_id,
        campaign_id,
        loan_code,
        loan_description,
        loan_product,
        loan_amount,
        funding_amount,
        deduction_amount,
        disbursement_amount,
        tenure,
        grace_period,
        borrower_grade,
        borrower_interest,
        borrower_interest_percentage,
        loan_status,
        latest_payment_status,
        lender_grade,
        lender_interest,
        flat_rate,
        lender_interest_percentage,
        cummulated_late_fee,
        sum_principal_paid,
        sum_interest_paid,
        sum_late_paid,
        sum_provision_fund_disbursed,
        is_affecting_limit,
        applied_at,
        funding_expired_at,
        finalized_at,
        is_borrower_lender_rate_diff,
        is_purchaseable_by_koin,
        is_splitted,
        is_parent_loan,
        parent_loan_id,
        is_top_up,
        is_eligible_for_top_up,
        is_resturctured,
        last_restructured_at,
        irr_borrower,
        irr_lender,
        ear_borrower,
        ear_lender,
        payment_type,
        longest_day_past_due,
        signing_type,
        digital_signing_product,
        transfer_date,
        system_transfer_date,
        is_active,
        created_by,
        created_at,
        modified_by,
        modified_at,
        deleted_by,
        deleted_at,
        source_type,
        disbursement_method,
        disbursement_scheme,
        repayment_process_type,
        committed_date,
        cancellation_type,
        repayment_process_status,
        vendor_name,
        vendor_code,
        average_rating,
        total_rating,
        total_lender_rating,
        borrower_rating,
        is_mdr,
        disbursement_target,
        is_preapproval,
        loan_origin,
        last_status_change,
        loss_acknowledged,
        loss_reason,
        product_variant_id,
        last_track_status,
        initial_tenure,
        initial_loan_amount,
        vendor_uuid,
        margin,
        prefund_rate,
        is_kg_resign,
        tenure_unit,
        "loan_type"
        from loans.loans 
        where loan_id = :loan_id
      `,{
          replacements: { loan_id: loan_id },
          type: QueryTypes.SELECT
        }
      ); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async findLoanRepaymentSchedulesbyLoanID(loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        SELECT
          lrs.installment_number,
          lrs.due_date,
          (SELECT ap.description FROM loans.admin_params ap WHERE ap.admin_param_uuid = lrs.installment_status) as installment_status
        FROM
          loans.loan_repayment_schedule_version lrsv
          JOIN loans.loan_repayment_schedules lrs ON lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id
            AND lrs.is_active = '1'
        WHERE
          lrsv.loan_id = :loan_id
          AND lrsv.is_current_version = '1'
          AND lrs.due_date::date <= CURRENT_DATE
        ORDER BY
          lrs.installment_number;
      `,{
          replacements: { loan_id: loan_id },
          type: QueryTypes.SELECT
        }
      ); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async findRejectedLoanbyLoanID(email, loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        WITH temp_rejected_table (loan_id) as
        (
          SELECT
            l.loan_id
          FROM
            loans.loans l
          WHERE
            l.user_id = (
              SELECT
                user_id
              FROM
                users.users u
              WHERE
                u.email = :email)
          AND l.loan_status = '7f25fba2-d916-11e9-97fa-00163e010bca'
          ORDER BY
            created_at DESC
          LIMIT 1
        )
        SELECT
          tulrd.top_up_loan_request_detail_id, tulrd.top_up_loan_request_id, tulrd.loan_id, tulrd.new_loan_id as rejected_loan_id, tulrd.top_up_request_status
        FROM
          loans.top_up_loan_request_detail tulrd, temp_rejected_table trt
        WHERE
          tulrd.new_loan_id = trt.loan_id 
          AND tulrd.loan_id  = :loan_id; 
      `,{
          replacements: { email: email, loan_id: loan_id }
        }
      ); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateLoanEligibleForTopUp(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        UPDATE
          loans.loans
        SET
          is_eligible_for_top_up = '1',
          modified_by = :ticket,
          modified_at = now()
        WHERE
          loan_id = :loan_id;
      `, { 
        replacements: { ticket: ticket, loan_id: loan_id },
        type: QueryTypes.UPDATE,
        transaction: t 
      }); //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async updateLoanTopupRejected(loan_id, loan_topup_request_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        UPDATE
          loans.top_up_loan_request_detail
        SET
          is_active = 0::bit,
          deleted_by = :ticket,
          deleted_at = now()
        WHERE
          loan_id = :loan_id;
        
        UPDATE
          loans.top_up_loan_request
        SET
          is_active = 0::bit,
          deleted_by = :ticket,
          deleted_at = now()
        WHERE
          top_up_loan_request_id = :loan_topup_request_id;
      `, { 
        replacements: { ticket: ticket, loan_id: loan_id, loan_topup_request_id: loan_topup_request_id },
        type: QueryTypes.UPDATE,
        transaction: t 
      }); //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async findLoansRepaymentSchedules() {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          lrs.schedule_id, 
          l.loan_code, 
          l.loan_id, 
          case when lrs.late_collectible > 0 then '95872c74-de79-11e9-97fa-00163e010bca'::uuid else '95872c71-de79-11e9-97fa-00163e010bca'::uuid end as valid_installment_status, 
          lrs.installment_status 
        from 
          loans.loans l 
          join loans.loan_repayment_schedule_version lrsv on l.loan_id = lrsv.loan_id 
          and lrsv.is_active = '1' 
          and lrsv.is_current_version = '1' 
          join loans.loan_repayment_schedules lrs on lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id 
          and lrs.is_active = '1' 
          and (
            (
              lrs.principal_collectible - lrs.sum_principal_paid
            ) + (
              lrs.interest_collectible - lrs.sum_interest_paid
            ) + (
              lrs.late_collectible - lrs.sum_late_paid
            )
          ) <= 0 
          and lrs.installment_status in (
            '95872c70-de79-11e9-97fa-00163e010bca', 
            '95872c72-de79-11e9-97fa-00163e010bca',
            '95872c73-de79-11e9-97fa-00163e010bca'
          ) 
          and lrs.due_date :: date <= current_date 
        where 
          l.is_active = '1' 
          and l.loan_status = '3b6b220b-2019-4279-8343-cbe6167f7574'
      `,{
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateRepaymentSchedule(ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update 
          loans.loan_repayment_schedules _lrs
        set 
          installment_status = sch.valid_installment_status, 
          modified_by = :ticket, 
          modified_at = now() 
        from 
          (
            select 
              lrs.schedule_id, 
              l.loan_code, 
              l.loan_id, 
              case when lrs.late_collectible > 0 then '95872c74-de79-11e9-97fa-00163e010bca'::uuid else '95872c71-de79-11e9-97fa-00163e010bca'::uuid end as valid_installment_status, 
              lrs.installment_status 
            from 
              loans.loans l 
              join loans.loan_repayment_schedule_version lrsv on l.loan_id = lrsv.loan_id 
              and lrsv.is_active = '1' 
              and lrsv.is_current_version = '1' 
              join loans.loan_repayment_schedules lrs on lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id 
              and lrs.is_active = '1' 
              and (
                (
                  lrs.principal_collectible - lrs.sum_principal_paid
                ) + (
                  lrs.interest_collectible - lrs.sum_interest_paid
                ) + (
                  lrs.late_collectible - lrs.sum_late_paid
                )
              ) <= 0 
              and lrs.installment_status in (
                '95872c70-de79-11e9-97fa-00163e010bca', 
                '95872c72-de79-11e9-97fa-00163e010bca',
                '95872c73-de79-11e9-97fa-00163e010bca'
              ) 
              and lrs.due_date :: date <= current_date 
            where 
              l.is_active = '1' 
              and l.loan_status = '3b6b220b-2019-4279-8343-cbe6167f7574'
          ) as sch 
        where 
          _lrs.schedule_id = sch.schedule_id;
      `, { 
        replacements: { ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t 
      }) //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async updateLenderSchedule(schedule_ids, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update 
          loans.lender_schedules 
        set 
          status_payment = sch.installment_status ,
          modified_by = :ticket, 
          modified_at = now() 
        from 
          (
            select 
              lrs.schedule_id, 
              lrs.installment_status 
            from 
              loans.loan_repayment_schedules lrs 
            where 
              lrs.schedule_id in (
                :schedule_ids
              )
          ) as sch 
        where 
          repayment_schedule_id = sch.schedule_id;
      `, { 
        replacements: { ticket: ticket, schedule_ids: schedule_ids },
        type: QueryTypes.UPDATE,
        transaction: t 
      }) //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async getLoanStatusbyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          l.loan_id,
          l.loan_code,
          ap_product.description as loan_product,
          ap_status.description as loan_status,
          l.applied_at 
        from loans.loans l
        left join loans.admin_params ap_status on ap_status.admin_param_uuid = l.loan_status
        left join loans.admin_params ap_product on ap_product.admin_param_uuid = l.loan_product 
        where l.is_active = 1::bit and
              l.loan_code = :loan_code;
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async updateLoanStatus(loan_id, loan_status, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update loans.loans 
        set loan_status = :loan_status,
          modified_by = :ticket,
          modified_at = now()
        where loan_id = :loan_id;
      `, { 
        replacements: { ticket: ticket, loan_id: loan_id, loan_status: loan_status },
        type: QueryTypes.UPDATE,
        transaction: t 
      }) //raw query execution

      await t.commit(); 
      return result;
    } catch (err) {
      await t.rollback();
      throw(err);
    }
  },
  async getLoanDisbursementDatebyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select
          l.loan_id,
          l.loan_code,
          ap_status.description as loan_status,
          l.transfer_date,
          l.system_transfer_date,
          (lrs.sum_principal_paid + lrs.sum_interest_paid + lrs.sum_late_paid) paid
        from
          loans.loans l
        join loans.loan_repayment_schedule_version lrsv on
          l.loan_id = lrsv .loan_id
          and lrsv.is_current_version = '1'
          and lrsv.is_active = '1'
        join loans.loan_repayment_schedules lrs on
          lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id
          and lrs.is_active = '1'
        join loans.admin_params ap_status on ap_status.admin_param_uuid = l.loan_status
        where
          l.loan_code = :loan_code
        limit 1;
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async getLoanRepaymentSchedulebyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select 
          lrs.installment_number,
          lrs.schedule_id,
          ap_status.description as installment_status,
          lrs.due_date,
          lrs.late_collectible,
          lrs.late_spreadable
        from 
          loans.loan_repayment_schedules lrs 
        left join loans.admin_params ap_status on lrs.installment_status = ap_status.admin_param_uuid 
        where 
          loan_repayment_schedule_version_id = (
            select 
              lrsv.loan_repayment_schedule_version_id 
            from
              loans.loans l 
              join loans.loan_repayment_schedule_version lrsv on l.loan_id = lrsv.loan_id 
              and lrsv.is_current_version = '1' 
              and lrsv.is_active = '1' 
              join loans.loan_repayment_schedules lrs ON lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id 
              and lrs.is_active = '1' 
            where 
              l.loan_code = :loan_code
            limit 1
        )
        order by lrs.schedule_id;
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async getLoanLenderSchedulebyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select
          ls.installment_number,
          ls.lender_schedule_id,
          ls.late_return,
          ap_status.description as status_payment,
          ls.due_date
        from 
          loans.lender_schedules ls 
        left join loans.admin_params ap_status on ap_status.admin_param_uuid = ls.status_payment 
        where 
          repayment_schedule_id IN (
            select 
              lrs.schedule_id 
            from 
              loans.loans l 
              join loans.loan_repayment_schedule_version lrsv on l.loan_id = lrsv.loan_id 
              and lrsv.is_current_version = '1' 
              and lrsv.is_active = '1' 
              join loans.loan_repayment_schedules lrs on lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id 
              and lrs.is_active = '1' 
            where 
              l.loan_code = :loan_code
            limit 1
        )
        order by ls.lender_schedule_id;
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  }
};

module.exports = { v1LoanService };