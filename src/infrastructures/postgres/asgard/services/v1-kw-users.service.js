const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1KWUserService = {
  async findDataUser(user_code) {
    try {
      const dbConn = getDBConn();
      const sql = `
        SELECT 
          user_id, user_code, fullname,
          email, phone_number,
          ap.param_group, ap.param_value,
          usr.is_active, 
          is_account_freezed,
          usr.created_by, usr.created_at, 
          usr.modified_by, usr.modified_at,
          usr.deleted_by, usr.deleted_at, 
          last_login_at 
        FROM users.users usr
        INNER JOIN users.admin_params ap 
        ON ap.admin_param_uuid = usr.account_type 
        WHERE user_code = :user_code ;`;
      result = await dbConn.query(sql, {
        replacements: { user_code: user_code },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  },
  async updateFullNameUser(user_code, fullname, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      var today = new Date().toISOString().slice(0, 10);
      const modified_by = 'TO_' + today + '_' + ticket;
      const sql = `
        UPDATE users.users 
        SET
          fullname = :fullname,
          modified_by = :modified_by,
          modified_at = now()
        WHERE user_code = :user_code ;`;
      result = await dbConn.query(sql, {
        replacements: { fullname: fullname, modified_by: modified_by, user_code: user_code },
        type: QueryTypes.UPDATE,
        transaction: t
      });
      await t.commit();
      return result;
    } catch (err) {
      await t.rollback();
      throw (err);
    }
  },
  async unfreezeAccountUser(user_code, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      var today = new Date().toISOString().slice(0, 10);
      const modified_by = 'TO_' + today + '_' + ticket;
      const sql = `
        UPDATE users.users 
        SET
          is_account_freezed  = '0',
          modified_by  = :modified_by,
          modified_at  = now()
        WHERE user_code = :user_code ;`;
      result = await dbConn.query(sql, {
        replacements: { modified_by: modified_by, user_code: user_code },
        type: QueryTypes.UPDATE,
        transaction: t
      });
      await t.commit();
      return result;
    } catch (err) {
      await t.rollback();
      throw (err);
    }
  }
};

module.exports = { v1KWUserService };