const { v1ExampleService } = require('./v1-example.services');
const { v1ForceVATService } = require('./v1-forcevat.services');
const { v1EmailService } = require('./v1-email.services');
const { v1LoanService } = require('./v1-loan.services');
const { v1BalanceUsersService } = require('./v1-balance-users.service');
const { v1MutationUsersService } = require('./v1-mutation-users.services');
const { v1LoanUserService } = require('./v1-loan-users.service');
const { v1KWUserService } = require('./v1-kw-users.service');
const { v1AssessmentService } = require('./v1-assessment.services');
const { v1ActivityService } = require('./v1-activity.services');
const { v1PortfolioService } = require('./v1-portfolio.services');

module.exports = {
    v1ExampleService,
    v1ForceVATService,
    v1EmailService,
    v1LoanService,
    v1BalanceUsersService,
    v1MutationUsersService,
    v1LoanUserService,
    v1KWUserService,
    v1AssessmentService,
    v1ActivityService,
    v1PortfolioService
};
