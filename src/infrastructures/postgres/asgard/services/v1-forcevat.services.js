const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1ForceVATService = {
  async findLoanDeductionDetailsByLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select * 
        from loans.loan_deduction_details ldd 
        where loan_id = 
        (select loan_id from loans.loans where loan_code = :loan_code) 
        and deduction_component_name = 'vat_fee' 
        and is_active = '1';
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  },
  async findInternalTransfersDetailsByLoanID(loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select * 
        from activity.internal_transfer_details itd 
        where is_active = '1' and internal_transfer_id = 
        (select internal_transfer_id from activity.internal_transfers it where loan_id = :loan_id)
        and detail_group = 'loan'
        and detail_name = 'vat_fee_percentage';
      `,{
        replacements: { loan_id: loan_id },
        type: QueryTypes.SELECT
      }) //raw query execution
      return result;
    } catch (err) {
      throw (err);
    }
  },
  async findRequestHeadersByLoanID(loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select ap.param_group, ap.name as in_out_type_value, ciorh.* 
        from 
          activity.cash_in_out_request_headers ciorh 
        left join 
          activity.admin_params ap 
          on ciorh.in_out_type = ap.admin_param_uuid 
        where ciorh.reference_id = :loan_id;
      `,{
        replacements: { loan_id: loan_id },
        type: QueryTypes.SELECT
      }) //raw query execution
      return result;
    } catch (err) {
      throw (err);
    }
  },
  async findVATFeeInRequestDetailsByLoanID(loan_id) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select * 
        from 
            activity.cash_in_out_request_details ciord 
        where ciord.in_out_header_id = 
          (select ciorh.in_out_header_id 
          from activity.cash_in_out_request_headers ciorh 
          where ciorh.reference_id = :loan_id)
        and detail_group = 'outflow_transfer'
        and detail_name = 'vat_fee';
      `,{
        replacements: { loan_id: loan_id },
        type: QueryTypes.SELECT
      }) //raw query execution
      return result;
    } catch (err) {
      throw (err);
    }
  },
  //Update Script
  async updateLoanDeductionDetails(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update loans.loan_deduction_details ldd
        set
          is_active = '0',
          deleted_by = :ticket,
          deleted_at = now()
        where 
          loan_id = :loan_id 
        and deduction_component_name = 'vat_fee' 
        and is_active = '1';
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t
      }) //raw query execution
      await t.commit()
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  async updateDeductionAndDisbursementAmount(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update
          loans.loans l 
        set
          deduction_amount = deduc.deduction_amount,
          disbursement_amount = deduc.disbursement_amount ,
          modified_by = :ticket,
          modified_at = now()
        from
          (
            select
              dd.loan_id,
              l.funding_amount ,
              sum(dd.deduction_amount) deduction_amount,
              l.funding_amount - sum(dd.deduction_amount) disbursement_amount
            from
              loans.loan_deduction_details dd
            join loans.loans l on
              dd.loan_id = l.loan_id
              and l.is_active = '1'
            where
              dd.loan_id = :loan_id
              and dd.is_active = '1'
            group by
              dd.loan_id,
              l.funding_amount 
          ) as deduc
        where
          l.loan_id = deduc.loan_id and l.loan_id = :loan_id;
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  async updateInternalTransferDetails(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update activity.internal_transfer_details itd 
        set
          is_active = '0',
          deleted_by = :ticket,
          deleted_at = now()
        where internal_transfer_id =
          (select internal_transfer_id from activity.internal_transfers it where loan_id = :loan_id)
        and detail_group = 'loan'
        and detail_name = 'vat_fee_percentage'
        and is_active = '1';
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  async updateCashInOutDetails(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        update activity.cash_in_out_request_details ciord
        set
          is_active = '0',
          deleted_by = :ticket,
          deleted_at = now()
        where 
          ciord.in_out_header_id = 
            (select ciorh.in_out_header_id 
          from activity.cash_in_out_request_headers ciorh 
          where ciorh.reference_id = :loan_id)
        and detail_group = 'outflow_transfer'
        and detail_name = 'vat_fee'
          and is_active = '1';
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.UPDATE,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  //Insert Script
  async insertLoanDeductionDetails(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        insert into loans.loan_deduction_details (loan_id, deduction_component_name, deduction_amount, is_active, created_by, created_at, modified_by, modified_at, deleted_by, deleted_at)
        values (
          <loan_id>, 
          'vat_fee',
          (select
            (dd.deduction_amount)*(0.11) actual_vat_fee
          from
            loans.loan_deduction_details dd
          join loans.loans l on
            dd.loan_id = l.loan_id
            and l.is_active = '1'
          where
            dd.loan_id = :loan_id
            and dd.is_active = '1'
            and deduction_component_name != 'vat_fee' 
          group by
            dd.loan_id,
            l.funding_amount
          ),
          '1',
          :ticket,
          now(),
          :ticket,
          now(),
          null,
          null
        );
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.INSERT,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  async insertInternalTransferDetails(loan_id, ticket) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        insert into activity.internal_transfer_details 
        (internal_transfer_id, detail_group, detail_name, detail_value, is_active, created_by, created_at, modified_by, modified_at, deleted_by, deleted_at)
        values (
          (select it.internal_transfer_id from activity.internal_transfers it where it.loan_id = '${loan_id}'),
          'loan',
          'vat_fee_percentage',
          (
            select
              round(dd.deduction_amount/l.funding_amount,7) as vat_fee_persentage
            from
              loans.loan_deduction_details dd
            join loans.loans l on
              dd.loan_id = l.loan_id
              and l.is_active = '1'
            where
              dd.loan_id = :loan_id
              and dd.is_active = '1'
              and dd.deduction_component_name = 'vat_fee' 
          ),
          '1',
          :ticket,
          now(),
          :ticket,
          now(),
          null,
          null
        );
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.INSERT,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
  async insertCashInOutRequestDetails(loan_id) {
    const dbConn = getDBConn();
    const t = await dbConn.transaction();
    try {
      result = await dbConn.query(`
        insert into activity.cash_in_out_request_details (in_out_header_id, detail_group, detail_name, detail_value, is_active, created_by, created_at, modified_by, modified_at, deleted_by, deleted_at)
        values (
          (select ciorh.in_out_header_id from activity.cash_in_out_request_headers ciorh where ciorh.reference_id ='${loan_id}'),
          'outflow_transfer',
          'vat_fee',
          (
            select
              dd.deduction_amount 
            from
              loans.loan_deduction_details dd
            join loans.loans l on
              dd.loan_id = l.loan_id
              and l.is_active = '1'
            where
              dd.loan_id = :loan_id
              and dd.is_active = '1'
              and deduction_component_name = 'vat_fee' 
          ),
          '1',
          :ticket,
          now(),
          :ticket,
          now(),
          null,
          null
        );
      `,{
        replacements: { loan_id: loan_id, ticket: ticket },
        type: QueryTypes.INSERT,
        transaction: t
      }) //raw query execution
      t.commit();
      return result;
    } catch (err) {
      t.rollback();
      throw (err);
    }
  },
};

module.exports = { v1ForceVATService };