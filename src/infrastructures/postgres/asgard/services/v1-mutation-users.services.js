const { QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const select = `SELECT 
                usr.user_id, usr.user_code, usr.email,
                am.mutation_id, am.mutation_date, am.description_id,
                am.cash_amount, am.before_cash_balance, am.after_cash_balance,
                am.koin_amount, am.before_koin_balance, am.after_koin_balance,
                am.is_locked,
                am.modified_by, am.modified_at,
                am.created_by, am.created_at, am.settlement_date `;
const from = ` FROM activity.account_mutations am
            INNER JOIN users.users usr
            ON am.user_id = usr.user_id`;

const v1MutationUsersService = {
  async findMutationUser(user_code, limit, param) {
    try {
      let start_date = ``;
      if (param.start_date) {
        start_date = ` AND am.mutation_date >= '${param.start_date}' `;
      }
      let is_locked = ``;
      if (param.is_locked) {
        is_locked = ` AND is_locked = '${param.is_locked}' `;
      }
      const dbConn = getDBConn();
      const sql = select + from + `
                  WHERE
                    usr.user_code = :user_code AND
                    am.is_active = '1'
                    :start_date 
                    :is_locked
                  ORDER BY mutation_date, mutation_id LIMIT :limit`;
      result = await dbConn.query(sql, {
        replacements: { user_code: user_code, limit: limit, start_date: start_date, is_locked: is_locked },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  },
  async findLastTransactionUser(user_code, limit) {
    try {
      const dbConn = getDBConn();
      const sql = select + from + `
                  WHERE
                    usr.user_code = :user_code AND
                    am.is_active = '1'
                  ORDER BY mutation_date, old_mutation_id, mutation_id LIMIT :limit`;
      result = await dbConn.query(sql, {
        replacements: { user_code: user_code, limit: limit },
        type: QueryTypes.SELECT
      });
      return result;
    } catch (err) {
      throw (err);
    }
  }
};

module.exports = { v1MutationUsersService };