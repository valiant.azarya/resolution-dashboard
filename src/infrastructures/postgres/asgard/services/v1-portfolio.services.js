const { Sequelize, QueryTypes } = require("sequelize");
const { getDBConn } = require("../../../../configs/postgres.config");

const v1PortfolioService = {
  async getRepaymentSchedulebyLoanCode(loan_code) {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query(`
        select
          rs.installment_number,
          rs.schedule_id,
          ap_status.description as status,
          rs.due_date,
          rs.late
        from 
          portfolio.repayment_schedules rs 
        left join portfolio.admin_params ap_status on ap_status.admin_param_uuid = rs.status 
        where 
          lender_schedule_id in (
            select 
              lender_schedule_id 
            from 
              loans.lender_schedules 
            where 
              repayment_schedule_id in (
                select 
                  schedule_id 
                from 
                  loans.loan_repayment_schedules lrs 
                where 
                  lrs.loan_repayment_schedule_version_id = (
                    select 
                      lrsv.loan_repayment_schedule_version_id 
                    from 
                      loans.loans l 
                      join loans.loan_repayment_schedule_version lrsv on l.loan_id = lrsv.loan_id 
                      and lrsv.is_current_version = '1' 
                      and lrsv.is_active = '1' 
                      join loans.loan_repayment_schedules lrs on lrsv.loan_repayment_schedule_version_id = lrs.loan_repayment_schedule_version_id 
                      and lrs.is_active = '1' 
                    where 
                      l.loan_code = :loan_code
                    limit 1
                  )
              )
          )
          order by rs.installment_number;
      `,{
        replacements: { loan_code: loan_code },
        type: QueryTypes.SELECT
      }); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  }
};

module.exports = { v1PortfolioService };