const { getDBConn } = require("../../../../configs/postgres.config");

const v1ExampleService = {
  async findUsersWithEmptyNPWP() {
    try {
      const dbConn = getDBConn();
      result = await dbConn.query("select * from users where users.username = ''"); //raw query execution
      return result;
    } catch (err) {
      throw(err);
    }
  }
};

module.exports = { v1ExampleService };