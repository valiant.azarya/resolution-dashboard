const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1ForceVATUsecase } = require("../../applications/usecases");

const v1ForceVATRest = {
  async viewVATFee(req, res, next) {
    const body = req.body;

    try {
      response = await v1ForceVATUsecase.viewVATFee(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }

      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async viewVATPercentage(req, res, next) {
    const body = req.body;

    try {
      response = await v1ForceVATUsecase.viewVATPercentage(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }

      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async viewRequestHeaders(req, res, next) {
    const body = req.body;

    try {
      response = await v1ForceVATUsecase.viewRequestHeaders(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }

      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async viewRequestDetails(req, res, next) {
    const body = req.body;

    try {
      response = await v1ForceVATUsecase.viewRequestDetails(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }

      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
}

module.exports = {
  v1ForceVATRest
};