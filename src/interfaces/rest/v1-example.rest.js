const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1ExampleUsecase } = require("../../applications/usecases");

const v1ExampleRest = async (req, res, next) => {
  const body = req.body;

  try {
    response = await v1ExampleUsecase(body);
        
    const payload = {
      success: true, // success flag
      status: httpStatus.success, // status code
      data: {}, // response data
      message: messages.success, // response message
    }
    return responseJSON(req, res, payload);
  } catch (err) {
    next(err);
  }

  next();
};

module.exports = { v1ExampleRest };