const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1LoanReadyForTopupUsecase } = require("../../applications/usecases");

const v1LoanReadyForTopupRest = {
  async checkLoan(req, res, next) {
    const body = req.body;

    try {
      response = await v1LoanReadyForTopupUsecase.viewLoan(body);

      let responsesConcat = [];
      responsesConcat = responsesConcat.concat(response[0]);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: responsesConcat, // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async checkLoanID(req, res, next) {
    const body = req.body;

    try {
      response = await v1LoanReadyForTopupUsecase.viewLoanID(body);

      let responsesConcat = [];
      responsesConcat = responsesConcat.concat(response[0], response[1], response[2]);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: responsesConcat, // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async updateLoanEligibleForTopup(req, res, next) {
    const body = req.body;

    try {
      response = await v1LoanReadyForTopupUsecase.updateLoanEligibleForTopup(body);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  }
}

module.exports = { 
  v1LoanReadyForTopupRest
};