const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1PlatformActivityBalanceUseCase } = require("../../applications/usecases/v1-platform-activity-balance.usescase");

const v1ActivityBalanceUsersRest = {
  async v1ViewBalanceUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1PlatformActivityBalanceUseCase.viewBalanceUser(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  }
};

module.exports = { v1ActivityBalanceUsersRest };