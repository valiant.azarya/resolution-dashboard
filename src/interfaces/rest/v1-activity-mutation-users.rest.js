const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1PlatformActivityMutationUseCase } = require("../../applications/usecases/v1-platform-activity-mutation.usecase");

const v1ActivityMutationUsersRest = {
  async v1ViewLastMutationUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1PlatformActivityMutationUseCase.viewLastMutation(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  },
  async v1ViewMutationUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1PlatformActivityMutationUseCase.viewMutation(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  }
};

module.exports = { v1ActivityMutationUsersRest };