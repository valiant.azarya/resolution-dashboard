const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");

const v1PingRest = async (req, res) => {
  const payload = {
    success: true, // success flag
    status: httpStatus.success, // status code
    data: {}, // response data
    message: messages.healthy, // response message
  }
  return responseJSON(req, res, payload);
};

module.exports = { v1PingRest };