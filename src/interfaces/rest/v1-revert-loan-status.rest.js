const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1RevertLoanStatusUsecase } = require("../../applications/usecases");
const { logger } = require("../../configs/logger.config");

const v1RevertLoanStatusRest = {
  async checkLoanStatus(req, res, next) {
    const body = req.body;

    try {
      response = await v1RevertLoanStatusUsecase.viewLoanStatus(body);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response, // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async updateLoanStatus(req, res, next) {
    const body = req.body;

    try {
      response = await v1RevertLoanStatusUsecase.updateLoanStatus(body);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  }
}

module.exports = { 
  v1RevertLoanStatusRest
};