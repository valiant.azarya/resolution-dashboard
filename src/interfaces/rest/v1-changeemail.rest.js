const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1ChangeEmailUsecase } = require("../../applications/usecases");

const v1ChangeEmailRest = {
  async viewEmail(req, res, next) {
    const body = req.body;

    try {
      response = await v1ChangeEmailUsecase.viewEmail(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async updateEmail(req, res, next) {
    const body = req.body;

    try {
      response = await v1ChangeEmailUsecase.updateEmail(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  }
}

module.exports = {
  v1ChangeEmailRest
};