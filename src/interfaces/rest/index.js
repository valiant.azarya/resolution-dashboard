const { v1ExampleRest } = require('./v1-example.rest');
const { v1PingRest } = require('./v1-ping.rest');
const { errorHandlerRest } = require('./error_handler.rest');
const { defaultHandlerRest } = require('./default_handler.rest');
const { v1ForceVATRest } = require('./v1-forcevat.rest');
const { v1ChangeEmailRest } = require('./v1-changeemail.rest');
const { v1LoanReadyForTopupRest } = require('./v1-loanreadyfortopup.rest');
const { v1UpdateInstallmentStatusRest } = require('./v1-updateinstallmentstatus.rest');
const { v1ActivityBalanceUsersRest } = require('./v1-activity-balance-users.rest');
const { v1ActivityMutationUsersRest } = require('./v1-activity-mutation-users.rest');
const { v1LoanStatusUsersRest } = require('./v1-loan-status-users.rest');
const { v1KWUsersRest } = require('./v1-kw-users.rest');
const { v1RevertLoanStatusRest } = require('./v1-revert-loan-status.rest');
const { v1ChangeDisbursementAndDueDateRest } = require('./v1-change-disbursement-and-due-date.rest');

module.exports = {
    v1ExampleRest,
    v1PingRest,
    errorHandlerRest,
    defaultHandlerRest,
    v1ForceVATRest,
    v1ChangeEmailRest,
    v1LoanReadyForTopupRest,
    v1UpdateInstallmentStatusRest,
    v1ActivityBalanceUsersRest,
    v1ActivityMutationUsersRest,
    v1LoanStatusUsersRest,
    v1KWUsersRest,
    v1RevertLoanStatusRest,
    v1ChangeDisbursementAndDueDateRest
};
