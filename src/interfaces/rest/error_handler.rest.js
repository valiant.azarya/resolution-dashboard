const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { logger } = require("../../configs/logger.config");

const errorHandlerRest = async (err, req, res, next) => {
  payload = {
    success: false, // success flag
    status: httpStatus.internalServerError, // status code
    message: messages.internalServerError, // response message
    err: err.toString()
  };
    
  logger().error(`ERR: ${err}`);
  return responseJSON(req, res, payload);
};

module.exports = { errorHandlerRest };