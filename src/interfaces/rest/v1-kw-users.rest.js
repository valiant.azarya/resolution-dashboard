const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1KWUsersUseCase } = require("../../applications/usecases/v1-kw-users.usecase");

const v1KWUsersRest = {
  async v1ViewDataUsers(req, res, next) {
    const body = req.body;
    try {
      const response = await v1KWUsersUseCase.viewDataKWUsers(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  },
  async v1UpdateFullNameUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1KWUsersUseCase.updateFullNameUser(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  },
  async v1UnfreezeAccountUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1KWUsersUseCase.unfreezeAccountUser(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  },
};

module.exports = { v1KWUsersRest };