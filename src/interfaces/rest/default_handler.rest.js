const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { logger } = require("../../configs/logger.config");

const defaultHandlerRest = async (req, res) => {
  payload = {
    success: false, // success flag
    status: httpStatus.notFound, // status code
    message: messages.pathNotFound, // response message
  };
    
  logger().warn(`WARN: ${messages.pathNotFound}`);
  return responseJSON(req, res, payload);
};

module.exports = { defaultHandlerRest };