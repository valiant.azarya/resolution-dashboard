const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1ChangeDisbursementAndDueDateUsecase } = require("../../applications/usecases");

const v1ChangeDisbursementAndDueDateRest = {
  async checkDisbursementAndDueDate(req, res, next) {
    const body = req.body;

    try {
      response = await v1ChangeDisbursementAndDueDateUsecase.viewLoanDisbursementAndDueDate(body);

      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response, // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  }
}

module.exports = {
  v1ChangeDisbursementAndDueDateRest
};
