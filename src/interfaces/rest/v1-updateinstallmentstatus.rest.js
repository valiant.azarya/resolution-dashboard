const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1UpdateInstallmentStatusUsecase } = require("../../applications/usecases");

const v1UpdateInstallmentStatusRest = {
  async viewInstallmentStatus(req, res, next) {
    const body = req.body;

    try {
      response = await v1UpdateInstallmentStatusUsecase.viewLoansRepaymentSchedules(body);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  },
  async updateInstallmentStatus(req, res, next) {
    const body = req.body;

    try {
      response = await v1UpdateInstallmentStatusUsecase.updateInstallmentStatus(body);
          
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: response[0][0], // response data
        message: messages.success, // response message
      }
      return responseJSON(req, res, payload);
    } catch (err) {
      next(err);
    }

    next();
  }
}

module.exports = { 
  v1UpdateInstallmentStatusRest
};