const { responseJSON } = require("../../helpers/http-response");
const { httpStatus, messages } = require("../../helpers/constants");
const { v1KoinP2PLoanStatusUserUseCase } = require("../../applications/usecases/v1-koinp2p-loan-status-user.usecase");

const v1LoanStatusUsersRest = {
  async v1ViewLoanStatusUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1KoinP2PLoanStatusUserUseCase.viewLoanStatusUser(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  },
  async v1ViewLoanFundingsUser(req, res, next) {
    const body = req.body;
    try {
      const response = await v1KoinP2PLoanStatusUserUseCase.viewLoanFundingsUser(body);
      const payload = {
        success: true, // success flag
        status: httpStatus.success, // status code
        data: { response }, // response data
        message: messages.healthy, // response message
      }
      return responseJSON(req, res, payload);

    } catch (err) {
      next(err);
    }
    next()
  }
};

module.exports = { v1LoanStatusUsersRest };